﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerColor : MonoBehaviour
{
    [SerializeField]
    private Material[] materials = null;
    private MeshRenderer[] renderers = null;

    private void Awake()
    {
        renderers = GetComponentsInChildren<MeshRenderer>();
    }

    public void SetPlayerColor(int materialIndex)
    {
        foreach (MeshRenderer meshRenderer in renderers)
        {
            meshRenderer.material = materials[materialIndex];
        }
    }
}
