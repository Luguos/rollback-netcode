﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : NetworkBehaviour, IPlayerAction
{
    private int animationState = 1;
    public MoveMessage moveMessage = null;

    private void Awake()
    {
        GetComponent<PlayerBehaviour>().RegisterAction(GetType(), this);
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        if (isLocalPlayer)
        {
            NetworkServer.RegisterHandler<MoveMessage>(OnReceiveServer);
        }
    }

    private void OnReceiveServer(NetworkConnection conn, MoveMessage message)
    {
        NetworkServer.SendToAll(message);
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (isLocalPlayer)
        {
            NetworkClient.RegisterHandler<MoveMessage>(OnReceiveClient);
        }
    }

    private void OnReceiveClient(NetworkConnection conn, MoveMessage message)
    {
        if (message.netId != netId)
        {
            int frameDifference = FrameCount.GetFrameNumber() - message.frameNumber;
            if (frameDifference == 0)
            {
                ProcessMessage(message);
            }
            else
            {
                RollbackManager.instance.ProcessInput(frameDifference, () => ProcessMessage(message));
            }
        }
    }

    private void ProcessMessage(MoveMessage message)
    {
        PlayerBehaviour inputPlayer = NetworkIdentity.spawned[message.netId].GetComponent<PlayerBehaviour>();
        inputPlayer.SetAction(Type.GetType(message.action), message.netId, message.target);
    }

    public void PrepareMessage(uint netId, params object[] args)
    {
        moveMessage = new MoveMessage();
        moveMessage.netId = netId;
        moveMessage.action = GetType().Name;
        moveMessage.target = (Vector3)args[0];
    }

    public void Send()
    {
        moveMessage.frameNumber = FrameCount.GetFrameNumber();
        NetworkClient.Send(moveMessage);
    }

    public void Execute()
    {
        PlayerBehaviour playerBehaviour = GetComponent<PlayerBehaviour>();
        Vector3 targetPosition = new Vector3(moveMessage.target.x, transform.position.y, moveMessage.target.z);        
        if (transform.position == targetPosition)
        {
            playerBehaviour.SetAction(typeof(PlayerIdle), moveMessage.netId);
        }
        else
        {
            playerBehaviour.RotateTo(targetPosition);
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, 5f * Time.deltaTime);
        }
    }

    public void SetAnimation(Animator animator)
    {
        animator.SetInteger("State", animationState);
    }

    public string Serialize()
    {
        MoveData data = new MoveData();
        data.netId = moveMessage.netId;
        data.target = moveMessage.target;
        return JsonUtility.ToJson(data);
    }

    public void Restore(string json)
    {
        MoveData data = JsonUtility.FromJson<MoveData>(json);
        moveMessage.netId = data.netId;
        moveMessage.target = data.target;
    }

    public class MoveData
    {
        public uint netId;
        public Vector3 target; 
    }
}
