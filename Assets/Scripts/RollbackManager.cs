﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollbackManager : MonoBehaviour
{
    public static RollbackManager instance = null;
    public Action simulateObjects = null;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void ProcessInput(int frameDifference, Action processAction)
    {
        Debug.Log(string.Format("Frame arrived with {0} difference!", frameDifference));
        if (frameDifference > 0)
        {
            if (frameDifference > GameStateSerializer.instance.frameAmount)
            {
                Debug.LogError(string.Format("Frame amount not supported: {0}", frameDifference));
                return;
            }

            GameStateSerializer.instance.Restore(FrameCount.GetFrameNumber() - frameDifference);
            processAction();
            for (int i = 0; i < frameDifference; i++)
            {
                simulateObjects();
            }
        }
        else
        {
            StartCoroutine(DelayInput(frameDifference, processAction));
        }
    }

    private IEnumerator DelayInput(int frames, Action processAction)
    {
        while (frames < 0)
        {
            frames++;
            yield return null;
        }
        processAction();
    }
}
