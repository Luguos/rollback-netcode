﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehaviour : MonoBehaviour, IStateSerializable
{
    [SerializeField]
    private float projectileSpeed = 0f;
    private Vector3 targetPosition = Vector3.zero;

    void Awake()
    {
        GameStateSerializer.instance.RegisterObject(this);
        RollbackManager.instance.simulateObjects += Simulate;
    }

    void Update()
    {
        Simulate();
    }

    public void Simulate()
    {
        if (transform.position == targetPosition)
        {
            gameObject.SetActive(false);
        }
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, projectileSpeed * Time.deltaTime);
    }

    public void SetTargetPosition(Vector3 targetPosition)
    {
        this.targetPosition = targetPosition;
    }

    public Vector3 GetTargetPosition()
    {
        return targetPosition;
    }

    public string Serialize()
    {
        ProjectileData data = new ProjectileData();
        data.isActive = gameObject.activeSelf;
        data.position = transform.localPosition;
        data.rotation = transform.localRotation;
        data.scale = transform.localScale;
        data.projectileSpeed = projectileSpeed;
        data.targetPosition = targetPosition;
        return JsonUtility.ToJson(data);
    }

    public void Restore(string json)
    {
        ProjectileData data = JsonUtility.FromJson<ProjectileData>(json);
        gameObject.SetActive(data.isActive);
        transform.localPosition = data.position;
        transform.localRotation = data.rotation;
        transform.localScale = data.scale;
        projectileSpeed = data.projectileSpeed;
        targetPosition = data.targetPosition;
    }

    public class ProjectileData
    {
        public bool isActive;
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 scale;
        public float projectileSpeed;
        public Vector3 targetPosition;
    }
}
