﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class PlayerIdle : NetworkBehaviour, IPlayerAction
{
    private int animationState = 0;
    public InputMessage inputMessage = null;

    private void Awake()
    {
        GetComponent<PlayerBehaviour>().RegisterAction(GetType(), this);
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        if (isLocalPlayer)
        {
            NetworkServer.RegisterHandler<InputMessage>(OnReceiveServer);
        }
    }

    private void OnReceiveServer(NetworkConnection conn, InputMessage message)
    {
        NetworkServer.SendToAll(message);
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (isLocalPlayer)
        {
            NetworkClient.RegisterHandler<InputMessage>(OnReceiveClient) ;
        }
    }

    private void OnReceiveClient(NetworkConnection conn, InputMessage message)
    {
        if (message.netId != netId)
        {
            int frameDifference = FrameCount.GetFrameNumber() - message.frameNumber;
            if (frameDifference == 0)
            {
                ProcessMessage(message);
            }
            else
            {
                RollbackManager.instance.ProcessInput(frameDifference, () => ProcessMessage(message));
            }
        }
    }

    private void ProcessMessage(InputMessage message)
    {
        PlayerBehaviour inputPlayer = NetworkIdentity.spawned[message.netId].GetComponent<PlayerBehaviour>();
        inputPlayer.SetAction(Type.GetType(message.action), message.netId);
    }

    public void PrepareMessage(uint netId, params object[] args)
    {
        inputMessage = new InputMessage();
        inputMessage.netId = netId;
        inputMessage.action = GetType().Name;
    }

    public void Send()
    {
        inputMessage.frameNumber = FrameCount.GetFrameNumber();
        NetworkClient.Send(inputMessage);
    }

    public void Execute() { }

    public void SetAnimation(Animator animator)
    {
        animator.SetInteger("State", animationState);
    }

    public string Serialize()
    {
        IdleData data = new IdleData();
        data.netId = inputMessage.netId;
        return JsonUtility.ToJson(data);
    }

    public void Restore(string json)
    {
        IdleData data = JsonUtility.FromJson<IdleData>(json);
        inputMessage.netId = data.netId; 
    }

    public class IdleData
    {
        public uint netId;
    }
}
