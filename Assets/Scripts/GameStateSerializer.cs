﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

public class GameStateSerializer : MonoBehaviour
{
    public static GameStateSerializer instance = null;
    private Dictionary<int, GameState> gameStates = new Dictionary<int, GameState>();
    private List<IStateSerializable> serializeObjects = new List<IStateSerializable>();
    public int frameAmount = 0;
    public bool isPaused = false;

    public int currentFrameNumber;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (Time.timeScale == 1)
            {
                currentFrameNumber = FrameCount.GetFrameNumber();
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }

            isPaused = !isPaused;
        }

        if (!isPaused)
        {
            if (gameStates.Count >= frameAmount)
            {
                gameStates.Remove(FrameCount.GetFrameNumber() - frameAmount);
            }

            gameStates[FrameCount.GetFrameNumber()] = Serialize();
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.LeftBracket))
            {
                if (gameStates.ContainsKey(currentFrameNumber - 1))
                {
                    currentFrameNumber = currentFrameNumber - 1;
                    Restore(currentFrameNumber);
                }
            }
            if (Input.GetKeyDown(KeyCode.RightBracket))
            {
                if (gameStates.ContainsKey(currentFrameNumber + 1))
                {
                    currentFrameNumber = currentFrameNumber + 1;
                    Restore(currentFrameNumber);
                }
            }
        }
    }

    public void RegisterObject(IStateSerializable serializeObject)
    {
        serializeObjects.Add(serializeObject);
    }

    private GameState Serialize()
    {
        GameState gameState = new GameState();
        gameState.objectJsonList = new Dictionary<IStateSerializable, string>();
        foreach (IStateSerializable serializeObject in serializeObjects)
        {
            string jsonString = serializeObject.Serialize();
            gameState.objectJsonList.Add(serializeObject, jsonString);
        }
        return gameState;
    }

    public void Restore(int frameNumber)
    {
        if (!gameStates.ContainsKey(frameNumber))
        {
            Debug.LogError(string.Format("Frame number {0} does not exist in list of game states!", frameNumber));
            return;
        }

        GameState gameState = gameStates[frameNumber];
        foreach (KeyValuePair<IStateSerializable, string> objectJson in gameState.objectJsonList)
        {
            objectJson.Key.Restore(objectJson.Value);
        }
    }
}

public class GameState
{
    public Dictionary<IStateSerializable, string> objectJsonList;
}

public interface IStateSerializable {
    string Serialize();
    void Restore(string json);
}
