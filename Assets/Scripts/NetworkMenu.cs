﻿using Mirror;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkMenu : MonoBehaviour
{
    [SerializeField]
    private NetworkManager manager = null;
    [SerializeField]
    private Button hostButton = null;
    [SerializeField]
    private Button joinButton = null;
    [SerializeField]
    private Button playButton = null;
    [SerializeField]
    private Text playerListText = null;
    [SerializeField]
    private GameObject[] pages = null;

    private int pageNumber = 0;

    public void Start()
    {
        Time.timeScale = 0;
        hostButton.onClick.AddListener(OnHostButton);
        joinButton.onClick.AddListener(OnJoinButton);
        playButton.onClick.AddListener(OnPlayButton);
        playerListText.text = "";
        AdvancePage();
    }

    public void OnHostButton()
    {
        manager.StartHost();
        AdvancePage();
    }

    public void OnJoinButton()
    {
        manager.StartClient();
        playButton.gameObject.SetActive(false);
        AdvancePage();
    }

    public void OnPlayButton()
    {
        manager.ServerChangeScene("Main");
    }

    public void AdvancePage()
    {
        if (pageNumber > 0)
        {
            pages[pageNumber - 1].SetActive(false);
        }
        pages[pageNumber].SetActive(true);
        pageNumber++;
    }

    public void UpdatePlayerList(int numConnections)
    {
        playerListText.text = "";
        for (int i = 1; i <= numConnections - 1; i++)
        {
            playerListText.text += "Player " + i + "\n";
        }
    }
}
