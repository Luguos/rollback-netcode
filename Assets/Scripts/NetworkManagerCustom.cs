﻿using Mirror;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NetworkManagerCustom : NetworkManager
{
    [SerializeField]
    private NetworkMenu networkMenu = null;
    private List<uint> playerList = new List<uint>();

    public override void OnStartClient()
    {
        base.OnStartClient();
        NetworkClient.RegisterHandler<PlayerListMessage>(ReceivePlayerList);
        NetworkClient.RegisterHandler<PlayerConnectMessage>(ReceiveConnectionAmount);
    }

    public override void OnServerReady(NetworkConnection conn)
    {
        base.OnServerReady(conn);
        if (IsSceneActive("Menu"))
        {
            PlayerConnectMessage message = new PlayerConnectMessage();
            message.numConnections = NetworkServer.connections.Count;
            NetworkServer.SendToAll<PlayerConnectMessage>(message);
        }
    }

    public void ReceiveConnectionAmount(NetworkConnection conn, PlayerConnectMessage message)
    {
        networkMenu.UpdatePlayerList(message.numConnections + 1);
    }

    public override void OnServerSceneChanged(string sceneName)
    {
        base.OnServerSceneChanged(sceneName);
        if (IsSceneActive("Main"))
        {
            foreach (NetworkConnection conn in NetworkServer.connections.Values)
            {
                GameObject playerObject = Instantiate(playerPrefab);
                NetworkServer.AddPlayerForConnection(conn, playerObject);
                playerList.Add(playerObject.GetComponent<NetworkIdentity>().netId);
            }

            PlayerListMessage message = new PlayerListMessage();
            message.playerList = playerList;
            NetworkServer.SendToAll<PlayerListMessage>(message);
        }
    }

    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        base.OnClientSceneChanged(conn);
        if (IsSceneActive("Main"))
        {
            Time.timeScale = 1;
        }
    }

    public void ReceivePlayerList(NetworkConnection conn, PlayerListMessage message)
    {
        playerList = message.playerList;
        for (int i = 0; i < playerList.Count; i++)
        {
            GameObject playerObject = NetworkIdentity.spawned[playerList[i]].gameObject;
            playerObject.transform.position = startPositions[i].position;
            playerObject.GetComponent<PlayerColor>().SetPlayerColor(i);
        }
    }
}
