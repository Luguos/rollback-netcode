﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : NetworkBehaviour, IPlayerAction
{
    [SerializeField] private GameObject projectile = null;
    [SerializeField] private GameObject projectileGroup = null;
    private int animationState = 2;
    public AttackMessage attackMessage = null;
    private bool spawnBullet = false;

    private void Awake()
    {
        GetComponent<PlayerBehaviour>().RegisterAction(GetType(), this);
        projectileGroup = Instantiate(projectileGroup);
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        if (isLocalPlayer)
        {
            NetworkServer.RegisterHandler<AttackMessage>(OnReceiveServer);
        }
    }

    private void OnReceiveServer(NetworkConnection conn, AttackMessage message)
    {
        NetworkServer.SendToAll(message);
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (isLocalPlayer)
        {
            NetworkClient.RegisterHandler<AttackMessage>(OnReceiveClient);
        }
    }

    private void OnReceiveClient(NetworkConnection conn, AttackMessage message)
    {
        if (message.netId != netId)
        {
            int frameDifference = FrameCount.GetFrameNumber() - message.frameNumber;
            if (frameDifference == 0)
            {
                ProcessMessage(message);
            }
            else
            {
                RollbackManager.instance.ProcessInput(frameDifference, () => ProcessMessage(message));
            }
        }
    }

    private void ProcessMessage(AttackMessage message)
    {
        PlayerBehaviour inputPlayer = NetworkIdentity.spawned[message.netId].GetComponent<PlayerBehaviour>();
        inputPlayer.SetAction(Type.GetType(message.action), message.netId, message.netIdTarget);
    }

    public void PrepareMessage(uint netId, params object[] args)
    {
        attackMessage = new AttackMessage();
        attackMessage.netId = netId;
        attackMessage.action = GetType().Name;
        attackMessage.netIdTarget = (uint)args[0];
    }

    public void Send()
    {
        attackMessage.frameNumber = FrameCount.GetFrameNumber();
        NetworkClient.Send(attackMessage);
    }

    public void Execute()
    {
        Vector3 enemyPosition = NetworkIdentity.spawned[attackMessage.netIdTarget].transform.position;
        Vector3 targetPosition = new Vector3(enemyPosition.x, transform.position.y, enemyPosition.z);
        GetComponent<PlayerBehaviour>().RotateTo(targetPosition);
        if (spawnBullet)
        {
            GameObject usedProjectile = null;
            if (projectileGroup.transform.childCount > 0)
            {
                foreach (Transform child in projectileGroup.transform)
                {
                    GameObject childObject = child.gameObject;
                    if (!childObject.activeSelf)
                    {
                        usedProjectile = childObject;
                        usedProjectile.transform.position = transform.position;
                        usedProjectile.SetActive(true);
                    }
                }
            }
            if (!usedProjectile)
            {
                usedProjectile = Instantiate(projectile, transform.position, Quaternion.identity, projectileGroup.transform);
            }
            usedProjectile.GetComponent<ProjectileBehaviour>().SetTargetPosition(targetPosition);
            spawnBullet = false;
        }
    }

    public void SpawnBullet()
    {
        spawnBullet = true;
    }

    public void SetAnimation(Animator animator)
    {
        animator.SetInteger("State", animationState);
    }

    public string Serialize()
    {
        AttackData data = new AttackData();
        data.netId = attackMessage.netId;
        data.netIdTarget = attackMessage.netIdTarget;
        return JsonUtility.ToJson(data);
    }

    public void Restore(string json)
    {
        AttackData data = JsonUtility.FromJson<AttackData>(json);
        attackMessage.netId = data.netId;
        attackMessage.netIdTarget = data.netIdTarget;
    }

    public class AttackData
    {
        public uint netId;
        public uint netIdTarget;
    }
}
