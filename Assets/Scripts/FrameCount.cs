﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FrameCount : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI frameText = null;
    public static int startFrameNumber = 0;

    private void Awake()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
        Application.runInBackground = true;
        startFrameNumber = Time.frameCount;
    }

    private void Update()
    {
        if (GameStateSerializer.instance.isPaused)
        {
            frameText.text = string.Format("Frame: {0}", GameStateSerializer.instance.currentFrameNumber);
        }
        else
        {
            frameText.text = string.Format("Frame: {0}", GetFrameNumber());
        }
    }

    public static int GetFrameNumber()
    {
        return Time.frameCount - startFrameNumber;
    }
}
