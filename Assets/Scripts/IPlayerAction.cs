﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerAction : IStateSerializable
{
    void PrepareMessage(uint netId, params object[] args);
    void Send();
    void Execute();
    void SetAnimation(Animator animator);
}
