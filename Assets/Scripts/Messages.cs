﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerConnectMessage : MessageBase
{
    public int numConnections;
}

public class PlayerListMessage : MessageBase
{
    public List<uint> playerList;
}

public class InputMessage : MessageBase
{
    public int frameNumber;
    public uint netId;
    public string action;
}

public class MoveMessage : InputMessage
{
    public Vector3 target;
}

public class AttackMessage : InputMessage
{
    public uint netIdTarget;
}