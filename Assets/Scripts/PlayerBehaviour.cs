﻿using Mirror;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : NetworkBehaviour, IStateSerializable
{
    [SerializeField] private GameObject mouseMarkerPrefab = null;
    private GameObject mouseMarker = null;

    private Dictionary<Type, IPlayerAction> playerActions = new Dictionary<Type, IPlayerAction>();
    private IPlayerAction currentAction = null;

    private Vector3 cameraValues = Vector3.zero;
    private Vector3 actionTarget = Vector3.zero;
    private Animator animator = null;
    private uint playerNetId = 0;

    public void Start()
    {
        if (isLocalPlayer)
        {
            cameraValues = Camera.main.transform.position;
        }

        animator = GetComponent<Animator>();
        mouseMarker = Instantiate(mouseMarkerPrefab);
        mouseMarker.SetActive(false);
        playerNetId = GetComponent<NetworkIdentity>().netId;

        SetAction(typeof(PlayerIdle), playerNetId);

        GameStateSerializer.instance.RegisterObject(this);
        RollbackManager.instance.simulateObjects += Simulate;
    }

    public void Update()
    {
        if (isLocalPlayer)
        {
            Camera.main.transform.position = transform.position + cameraValues;

            // Handle mouse input
            if (Input.GetMouseButtonDown(0))
            {
                HandleClick();
                currentAction.Send();
            }
        }

        Simulate();
    }

    public void Simulate()
    {
        currentAction.SetAnimation(animator);
        currentAction.Execute();
    }

    public void HandleClick()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 50f))
        {
            actionTarget = hit.point;
            if (isLocalPlayer)
            {
                mouseMarker.SetActive(true);
                mouseMarker.transform.position = new Vector3(actionTarget.x, mouseMarker.transform.position.y, actionTarget.z);
            }

            if (hit.collider.CompareTag("Player"))
            {
                uint netIdCollider = hit.collider.gameObject.GetComponent<NetworkIdentity>().netId;
                if (netIdCollider != netId)
                {
                    SetAction(typeof(PlayerAttack), playerNetId, netIdCollider);
                }
            }
            else
            {
                SetAction(typeof(PlayerMove), playerNetId, actionTarget);
            }
        }
    }

    public void RotateTo(Vector3 rotatePoint)
    {
        Vector3 rotateDirection = (rotatePoint - transform.position).normalized;
        transform.rotation = Quaternion.LookRotation(rotateDirection);
    }

    public void RegisterAction(Type actionType, IPlayerAction actionBehaviour)
    {
        playerActions.Add(actionType, actionBehaviour);
    }

    public void SetAction(Type action, uint netId, params object[] args)
    {
        currentAction = playerActions[action];
        currentAction.PrepareMessage(netId, args);
    }

    public string Serialize()
    {
        PlayerData data = new PlayerData();
        data.position = transform.localPosition;
        data.rotation = transform.localRotation;
        data.scale = transform.localScale;
        data.action = currentAction.GetType().Name;
        data.actionJson = currentAction.Serialize();

        AnimatorStateInfo animationInfo = animator.GetCurrentAnimatorStateInfo(0);
        data.animationHash = animationInfo.shortNameHash;
        data.animationTime = animationInfo.normalizedTime;

        return JsonUtility.ToJson(data);
    }

    public void Restore(string json)
    {
        PlayerData data = JsonUtility.FromJson<PlayerData>(json);
        transform.localPosition = data.position;
        transform.localRotation = data.rotation;
        transform.localScale = data.scale;

        currentAction = playerActions[Type.GetType(data.action)];
        currentAction.Restore(data.actionJson);

        animator.Play(data.animationHash, 0, data.animationTime);
    }

    public class PlayerData
    {
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 scale;
        public string action;
        public string actionJson;
        public int animationHash;
        public float animationTime;
    }
}